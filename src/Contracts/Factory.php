<?php

namespace Xin\Crawler;

interface Factory
{
	/**
	 * 采集
	 * @param string $url
	 * @param array $httpConfig
	 * @return array
	 */
	public function craw($url, array $httpConfig = []);
}