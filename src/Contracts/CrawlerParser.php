<?php

namespace Xin\Crawler;

/**
 * 数据采集解析器
 *
 * @package xin\articlecollect
 */
interface CrawlerParser
{
	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data);

	/**
	 * 是否匹配
	 * @param array $urlInfo
	 * @return bool
	 */
	public static function isMatchUrl($urlInfo);

}
