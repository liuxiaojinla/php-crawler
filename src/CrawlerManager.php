<?php

namespace Xin\Crawler;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Xin\Capsule\WithConfig;
use Xin\Crawler\Parsers\BaijiahaoCrawlerParser;
use Xin\Crawler\Parsers\CNBlogCrawlerParser;
use Xin\Crawler\Parsers\CSDNCrawlerParser;
use Xin\Crawler\Parsers\JianshuCrawlerParser;
use Xin\Crawler\Parsers\QQNewsCrawlerParser;
use Xin\Crawler\Parsers\RenminwangCrawlerParser;
use Xin\Crawler\Parsers\RuanyifengCrawlerParser;
use Xin\Crawler\Parsers\SohuCrawlerParser;
use Xin\Crawler\Parsers\WechatOfficialCrawlerParser;

/**
 * 基础类
 */
class CrawlerManager implements Factory
{
	use WithConfig;

	/**
	 * @var array
	 */
	protected static $providerClass = [
		BaijiahaoCrawlerParser::class,
		CNBlogCrawlerParser::class,
		CSDNCrawlerParser::class,
		JianshuCrawlerParser::class,
		QQNewsCrawlerParser::class,
		RenminwangCrawlerParser::class,
		RuanyifengCrawlerParser::class,
		SohuCrawlerParser::class,
		WechatOfficialCrawlerParser::class,
	];

	/**
	 * @var array
	 */
	protected $parsers = [];

	/**
	 * 客户端默认配置
	 *
	 * @var array
	 */
	protected static $defaultClientConfig = [
		'verify' => false,
		'headers' => [
			'User-Agent' => 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.25 Safari/537.36 Core/1.70.3719.400 QQBrowser/10.5.3715.400',
		],
	];

	/**
	 * @var string[]
	 */
	protected static $httpCodeMessage = [
		200 => '服务器成功返回请求的数据。',
		201 => '新建或修改数据成功。',
		202 => '一个请求已经进入后台排队（异步任务）。',
		204 => '删除数据成功。',
		400 => '发出的请求有错误，服务器没有进行新建或修改数据的操作。',
		401 => '用户没有权限（令牌、用户名、密码错误）。',
		403 => '用户得到授权，但是访问是被禁止的。',
		404 => '发出的请求针对的是不存在的记录，服务器没有进行操作。',
		406 => '请求的格式不可得。',
		410 => '请求的资源被永久删除，且不会再得到的。',
		422 => '当创建一个对象时，发生一个验证错误。',
		500 => '服务器发生错误，请检查服务器。',
		502 => '网关错误。',
		503 => '服务不可用，服务器暂时过载或维护。',
		504 => '网关超时。',
	];

	/**
	 * @inheritDoc
	 * @throws \GuzzleHttp\Exception\GuzzleException
	 * @throws \Xin\Crawler\CrawlerParseException
	 * @throws \Xin\Crawler\CrawlerParserNotFoundException
	 */
	public function craw($url, array $httpConfig = [])
	{
		$provider = $this->resolveProviderOfURL($url);

		try {
			$httpConfig = array_replace_recursive(static::$defaultClientConfig, $httpConfig);
			$client = new Client($httpConfig);

			$response = $client->get($url);
			$contents = $response->getBody()->getContents();
		} catch (RequestException $e) {
			if ($e->getResponse()) {
				$httpStatusCode = $e->getResponse()->getStatusCode();
				throw new CrawlerParseException("解析失败：" . self::$httpCodeMessage[$httpStatusCode]);
			}

			if ($e instanceof ConnectException) {
				throw new CrawlerParseException("解析失败：网络连接失败({$e->getMessage()})");
			}

			throw new CrawlerParseException("解析失败：{$e->getMessage()}");
		}

		return $provider->parse($contents);
	}

	/**
	 * 解析采集器
	 * @param string $class
	 * @return \Xin\Crawler\CrawlerParser
	 */
	protected function resolverProvider($class)
	{
		if (!isset($this->parsers[$class])) {
			$this->parsers[$class] = new $class;
		}

		return $this->parsers[$class];
	}

	/**
	 * 根据url解析采集器提供者
	 * @param string $url
	 * @return string|\Xin\Crawler\CrawlerParser
	 * @throws \Xin\Crawler\CrawlerParserNotFoundException
	 */
	protected function resolveProviderOfURL($url)
	{
		$urlInfo = parse_url($url);

		/** @var \Xin\Crawler\CrawlerParser $providerClass */
		foreach (static::$providerClass as $providerClass) {
			if ($providerClass::isMatchUrl($urlInfo)) {
				return $providerClass;
			}
		}

		throw new CrawlerParserNotFoundException();
	}

	/**
	 * 添加提供者类
	 * @param string $class
	 * @return void
	 */
	public static function addProviderClass($class)
	{
		static::$providerClass[] = $class;
	}

	/**
	 * 移除提供者类
	 * @param string $class
	 * @return void
	 */
	public static function forgetProviderClass($class)
	{
		$index = array_search($class, static::$providerClass, true);
		if ($index !== false) {
			array_splice(static::$providerClass, $index, 1);
		}
	}
}
