<?php

namespace Xin\Crawler;

class CrawlerParserNotFoundException extends \Exception
{
	/**
	 * @inheritDoc
	 */
	public function __construct($code = 0, \Throwable $previous = null)
	{
		parent::__construct("解析器未找到！", $code, $previous);
	}
}
