<?php

namespace Xin\Crawler\Parsers;

use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

class SohuCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['www.sohu.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.text-title h1')->text();

		$createTime = $query->find('#news-time')->text();
		$createTime = strtotime($createTime);

		$viewCount = 0;

		$content = $query->find('.article');
		$content->find('[data-role="original-title"]')->remove();
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
