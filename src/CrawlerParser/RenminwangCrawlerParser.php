<?php

namespace Xin\Crawler\Parsers;

use QL\Dom\Elements;
use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

class RenminwangCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['money.people.com.cn'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data)->encoding('UTF-8', 'GB2312')->removeHead();
		$title = $query->find('h1')->text();

		$createTime = $query->find('.box01 .fl')->text();
		$createTime = explode('  ', $createTime)[0];
		$createTime = date_parse_from_format("Y年m月d日H:i", $createTime);
		$createTime = mktime(
			$createTime['hour'],
			$createTime['minute'],
			$createTime['second'],
			$createTime['month'],
			$createTime['day'],
			$createTime['year']
		);

		$viewCount = 0;

		$content = $query->find('.box_con');
		$content->find('img')->map(function (Elements $item) {
			$imgUrl = $item->attr('data-original-src');
			$item->removeAttr('data-original-src');
			if ($imgUrl) {
				$item->attr('src', $imgUrl);
			}
			return $item;
		});
		$content->remove('.edit.clearfix');
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
