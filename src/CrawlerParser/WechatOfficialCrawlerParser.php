<?php

namespace Xin\Crawler\Parsers;

use QL\Dom\Elements;
use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

/**
 * 微信公众号
 *
 * @package xin\articlecollect\driver
 */
class WechatOfficialCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['mp.weixin.qq.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.rich_media_title')->text();

		$content = $query->find('.rich_media_content');
		$content->find('img')->map(function (Elements $item) {
			$item->removeAttr('data-before-oversubscription-url');
			$imgUrl = $item->attr('data-src');
			$item->removeAttr('data-src');
			if ($imgUrl) {
				$item->attr('src', $imgUrl);
			}

			return $item;
		});
		$content = $content->html();

		$viewCount = 0;

		$createTime = time();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
