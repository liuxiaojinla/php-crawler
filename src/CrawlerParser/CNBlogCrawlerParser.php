<?php

namespace Xin\Crawler\Parsers;

use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

/**
 * 博客园
 */
class CNBlogCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['www.cnblogs.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('#cb_post_title_url')->text();

		$createTime = $query->find('#post-date')->text();
		$createTime = strtotime($createTime);

		$viewCount = $query->find('#post_view_count')->text();

		$content = $query->find('.blogpost-body');
		$content->find('p:eq(0)')->remove();
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
