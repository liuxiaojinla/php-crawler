<?php

namespace Xin\Crawler\Parsers;

use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

/**
 * CSDN
 */
class CSDNCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['blog.csdn.net'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.title-article')->text();

		$createTime = $query->find('.article-bar-top .time')->text();
		$createTime = date_parse_from_format("Y年m月d日 H:i:s", $createTime);
		$createTime = mktime(
			$createTime['hour'],
			$createTime['minute'],
			$createTime['second'],
			$createTime['month'],
			$createTime['day'],
			$createTime['year']
		);

		$viewCount = $query->find('.read-count')->text();
		$viewCount = explode(' ', $viewCount)[1];

		$content = $query->find('#content_views');
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
