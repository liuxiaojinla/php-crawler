<?php

namespace Xin\Crawler\Parsers;

use QL\Dom\Elements;
use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

class JianshuCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['www.jianshu.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.article .title')->text();

		$createTime = $query->find('.article .publish-time')->text();
		$createTime = date_parse_from_format("Y.m.d H:i", $createTime);
		$createTime = mktime(
			$createTime['hour'],
			$createTime['minute'],
			$createTime['second'],
			$createTime['month'],
			$createTime['day'],
			$createTime['year']
		);

		$viewCount = 0;

		$content = $query->find('.show-content-free');
		$content->find('img')->map(function (Elements $item) {
			$imgUrl = $item->attr('data-original-src');
			$item->removeAttr('data-original-src');
			if ($imgUrl) {
				$item->attr('src', $imgUrl);
			}

			return $item;
		});
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
