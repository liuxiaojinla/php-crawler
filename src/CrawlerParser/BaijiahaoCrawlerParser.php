<?php

namespace Xin\Crawler\Parsers;

use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

/**
 * 百家号
 */
class BaijiahaoCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['baijiahao.baidu.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.article-title')->text();

		$createTime = $query->find('.article-source-bjh .date,.article-source-bjh .time')->text();
		$createTime = date_parse_from_format("发布时间：Y-m-d\nH:i", $createTime);
		$createTime = mktime(
			$createTime['hour'],
			$createTime['minute'],
			$createTime['second'],
			$createTime['month'],
			$createTime['day'],
			$createTime['year']
		);

		$viewCount = 0;

		$content = $query->find('.article-content');
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
