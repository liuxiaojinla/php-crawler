<?php

namespace Xin\Crawler\Parsers;

use QL\Dom\Elements;
use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

class QQNewsCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['new.qq.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data)->encoding('UTF-8', 'GB2312')->removeHead();
		$title = $query->find('h1')->text();

		$viewCount = 0;

		$content = $query->find('.content-article');
		$content->find('img')->map(function (Elements $item) {
			$imgUrl = $item->attr('data-original-src');
			$item->removeAttr('data-original-src');
			if ($imgUrl) {
				$item->attr('src', $imgUrl);
			}
			return $item;
		});
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => time(),
		];
	}
}
