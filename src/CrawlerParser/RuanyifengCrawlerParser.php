<?php

namespace Xin\Crawler\Parsers;

use QL\Dom\Elements;
use QL\QueryList;
use Xin\Crawler\AbstractCrawlerParser;

class RuanyifengCrawlerParser extends AbstractCrawlerParser
{

	/**
	 * @inheritDoc
	 */
	protected static function getMatchDomains()
	{
		return ['www.ruanyifeng.com'];
	}

	/**
	 * 内容解析
	 *
	 * @param string $data HTML内容
	 * @return array
	 */
	public function parse($data)
	{
		$query = QueryList::html($data);
		$title = $query->find('.page-title')->text();

		$createTime = $query->find('.published')->attr('title');
		$createTime = strtotime($createTime);

		$viewCount = 0;

		$content = $query->find('#main-content');
		$content->find('img')->map(function (Elements $item) {
			$imgUrl = $item->attr('data-original-src');
			$item->removeAttr('data-original-src');
			if ($imgUrl) {
				$item->attr('src', $imgUrl);
			}

			return $item;
		});
		$content = $content->html();

		return [
			'title' => $title,
			'content' => $content,
			'view_count' => $viewCount,
			'create_time' => $createTime,
		];
	}
}
