<?php

namespace Xin\Crawler;

use Xin\Capsule\Service;

/**
 * 基础类
 */
abstract class AbstractCrawlerParser extends Service implements CrawlerParser
{
	/**
	 * @inheritDoc
	 */
	public static function isMatchUrl($urlInfo)
	{
		$domains = static::getMatchDomains();

		return in_array($urlInfo['host'], $domains, true);
	}

	/**
	 * @return string[]
	 */
	abstract protected static function getMatchDomains();
}
