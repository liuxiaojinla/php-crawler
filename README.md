# Crawler | 数据采集

#### 介绍

一个简单的API即可实现多平台文章采集，目前已接入：微信公众号，CSDN，简书，阮一峰日志，人民网，QQ新闻

#### 安装教程

```bash
composer require xin/crawler
```

#### 使用说明

```php
use Xin\Crawler\CrawlerManager;

require_once '../vendor/autoload.php';

$crawler = new CrawlerManager();

//$data = $crawler->craw('https://mp.weixin.qq.com/s/3vD2wppFR7Ljl4nO9p97uA');
//$data = $crawler->craw('https://blog.csdn.net/jimlong/article/details/8606005');
//$data = $crawler->craw('https://www.jianshu.com/p/871c604d9aa2');
//$data = $crawler->craw('http://www.ruanyifeng.com/blog/2019/08/information-theory.html');
//$data = $crawler->craw('http://money.people.com.cn/n1/2019/0805/c42877-31276626.html');
//$data = $crawler->craw('https://new.qq.com/omn/FIN20190/FIN2019080500948300.html');
//$data = $crawler->craw('https://www.cnblogs.com/mithrandirw/p/8468925.html');
//$data = $crawler->craw('https://baijiahao.baidu.com/s?id=1619247656903510608&wfr=spider&for=pc');
$data = $crawler->craw('https://www.sohu.com/a/223511457_99893391');
var_dump($data);
```
