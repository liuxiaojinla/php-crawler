<?php
/**
 * Talents come from diligence, and knowledge is gained by accumulation.
 *
 * @author: BD<liuxingwu@duoguan.com>
 * @date: 2019/8/4 23:43
 */

use xin\articlecollect\CrawlerManager;

require_once '../vendor/autoload.php';

//$data = ArticleCollect::url('https://mp.weixin.qq.com/s/3vD2wppFR7Ljl4nO9p97uA');
//$data = ArticleCollect::url('https://blog.csdn.net/jimlong/article/details/8606005');
//$data = ArticleCollect::url('https://www.jianshu.com/p/871c604d9aa2');
//$data = ArticleCollect::url('http://www.ruanyifeng.com/blog/2019/08/information-theory.html');
//$data = ArticleCollect::url('http://money.people.com.cn/n1/2019/0805/c42877-31276626.html');
//$data = ArticleCollect::url('https://new.qq.com/omn/FIN20190/FIN2019080500948300.html');
//$data = ArticleCollect::url('https://www.cnblogs.com/mithrandirw/p/8468925.html');
//$data = ArticleCollect::url('https://baijiahao.baidu.com/s?id=1619247656903510608&wfr=spider&for=pc');
$data = CrawlerManager::url('https://www.sohu.com/a/223511457_99893391');
var_dump($data);
